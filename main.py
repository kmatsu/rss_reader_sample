# coding: utf-8
import re, os, datetime, argparse, sys
import requests
from bs4 import BeautifulSoup

class Rss:
    DELETE_TEXTS = [r"NewsPicks"]
    OUTPUT_DIR_NAME = "output"
    def __init__(self, url):
        self.url = url
        self.items = []
    def read(self):
        res = requests.get(self.url)
        contents = res.content
        soup = BeautifulSoup(contents, 'html.parser')
        self.items = soup.find_all('item')
    def text_convert(self, text):
        text = self.text_delete(text)
        text = self.tag_delete(text)
        return text
    def text_delete(self, text):
        for pattern in self.DELETE_TEXTS:
            text = re.sub(pattern, "", text)
        return text   
    def tag_delete(self, text):
        return re.sub(r"<.*?>", "", text)   
    def linefeed_delete(self, text):
        return re.sub('[\r\n]', '', text)
    def run(self):
        self.read()
        if not os.path.exists(self.OUTPUT_DIR_NAME):
            os.mkdir(self.OUTPUT_DIR_NAME)
        dt_now = datetime.datetime.now()    
        file_path = "{}/{}.txt".format(self.OUTPUT_DIR_NAME, dt_now.strftime('%Y%m%d%H%M%S'))
        with open(file_path, "w") as f:
            for index, item in enumerate(self.items):
                title = self.text_convert(item.title.text)
                guid = item.guid.text
                description = self.text_convert(item.description.text)
                date = item.pubdate.text
                print("NO: {}".format(index))
                print("TITLE: {}".format(title))
                print("GUID: {}".format(guid))
                print("DESCRIPTION:")
                print(description)
                print("DATE: {}".format(date))
            
                f.write("{},{},{},{},{}\n".format(index, guid, title, self.linefeed_delete(description), date))
        print("OUTPUT FILE PATH: {}".format(file_path))

if __name__ == "__main__":
    print("--- start ---")
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--url', type=str, default="http://tech.uzabase.com/rss", help="RSS URL", required=False)
        args = parser.parse_args()
        rss = Rss(args.url)
        rss.run()
    except requests.exceptions.MissingSchema:
        print("--- [ERROR] missing url ---")    
    except:
        print("--- [ERROR] error occur ---")
        print(sys.exc_info()) 
    print("--- end ---")
    