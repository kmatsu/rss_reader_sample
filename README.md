# Rss reader(test version)
This is a simple rss converter.

# Usage
## setting your environment
```
python -m venv rss_test
source rss_test/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

## execute Rss reader
```
python main.py --url YOUR_RSS_URL
```
